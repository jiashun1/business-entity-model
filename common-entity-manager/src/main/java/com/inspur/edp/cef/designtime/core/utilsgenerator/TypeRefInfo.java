/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.core.utilsgenerator;

public class TypeRefInfo {
    private String typePackageName;
    private String typeName;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypePackageName() {
        return typePackageName;
    }

    public void setTypePackageName(String typePackageName) {
        this.typePackageName = typePackageName;
    }

    public TypeRefInfo(String typeName,String typePackageName)
    {
        this.typeName = typeName;
        this.typePackageName = typePackageName;
    }

    public TypeRefInfo(Class typeClass)
    {
        typeName=typeClass.getSimpleName();
        if(typeClass.getPackage()!=null)
        typePackageName=typeClass.getPackage().getName();
    }

    public void write(StringBuilder stringBuilder) {
        stringBuilder.append(getTypeName());
    }

    public String getFullName()
    {
        return getTypePackageName()+"."+getTypeName();
    }
}
