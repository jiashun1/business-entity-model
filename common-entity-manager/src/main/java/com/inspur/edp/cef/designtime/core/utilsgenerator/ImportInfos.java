/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.core.utilsgenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * import信息
 *
 * @author haoxiaofei
 */
public class ImportInfos {
    private List<String> importPackages = new ArrayList();

    /**
     * 获取import Package信息
     *
     * @return import Package信息
     */
    public List<String> getImportPackages() {
        return importPackages;
    }

    /**
     * 添加package
     *
     * @param value 类型
     */
    public void addImportPackage(String value) {
        if (importPackages.contains(value)) {
            return;
        }
        importPackages.add(value);
    }

    /**
     * 添加package
     *
     * @param typeRefInfo 类型
     */
    public void addImportPackage(TypeRefInfo typeRefInfo) {
        if ("void".equals(typeRefInfo.getTypeName())) {
            return;
        }
        if ("java.lang".equals(typeRefInfo.getTypePackageName())) {
            return;
        }
        if (typeRefInfo.getTypePackageName() == null || "".equals(typeRefInfo.getTypePackageName())) {
            return;
        }
        if (importPackages.contains(typeRefInfo.getFullName())) {
            return;
        }
        importPackages.add(typeRefInfo.getFullName());
    }

    /**
     * 拼接import
     *
     * @param stringBuilder import值
     */
    public void write(StringBuilder stringBuilder) {
        Collections.sort(getImportPackages());
        for (String value : getImportPackages()) {
            stringBuilder.append("import " + value + ";\n");
        }
    }
}
