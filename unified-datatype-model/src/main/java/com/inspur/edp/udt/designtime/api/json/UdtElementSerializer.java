
/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldSerializer;

public class UdtElementSerializer extends CefFieldSerializer {

	public UdtElementSerializer(){}
	public UdtElementSerializer(boolean full){
		super(full);
		isFull = full;
	}
	@Override
	protected void writeExtendFieldBaseProperty(JsonGenerator jsonGenerator, IGspCommonField iGspCommonField) {

	}

	@Override
	protected void writeExtendFieldSelfProperty(JsonGenerator writer, IGspCommonField field) {
		SerializerUtils.writePropertyValue(writer, UdtNames.Id, field.getID());
		SerializerUtils.writePropertyValue(writer, UdtNames.LabelId, field.getLabelID());
		if(isFull||(field.getRefElementId()!=null&&!"".equals(field.getRefElementId())))
			SerializerUtils.writePropertyValue(writer, UdtNames.RefElementId, field.getRefElementId());
		SerializerUtils.writePropertyValue(writer, UdtNames.DataType, field.getMDataType().toString());
		writeUnfiedDataType();
	}

	private void writeUnfiedDataType() {
		// N版-字段引用单值udt时，序列化到了字段属性中，J版暂不需要
	}
}
