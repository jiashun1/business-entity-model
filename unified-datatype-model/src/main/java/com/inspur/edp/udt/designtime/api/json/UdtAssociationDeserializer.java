/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.json;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.GspAssociationDeserializer;
import com.inspur.edp.udt.designtime.api.json.UdtElementDeserializer;
import com.inspur.edp.udt.designtime.api.json.UdtNames;

public class UdtAssociationDeserializer extends GspAssociationDeserializer {

    public UdtAssociationDeserializer(UdtElementDeserializer udtElementDeserializer) {
        super(udtElementDeserializer);
    }

    @Override
    public boolean ReadExtendAssoProperty(GspAssociation asso, String propName, JsonParser jsonParser) {
        boolean result = true;

        GspAssociation element = asso;
        switch (propName) {
            case UdtNames.Id:
                element.setId(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.RefEntityId:
                element.setRefModelID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.RefEntityPkgName:
                element.setRefModelPkgName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.RefEntityCode:
                element.setRefModelCode(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.RefEntityName:
                element.setRefModelName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            default:
                result = false;
        }
        return result;
    }
}
