/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.extension;

import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@PropertySource(value = {"application.yml"}, factory = io.iec.edp.caf.commons.event.config.CompositePropertySourceFactory.class)
@ConfigurationProperties(prefix = "udt-extension")
public class UdtExtensionConfigs {
    private static UdtExtensionConfigs instance;
    public static UdtExtensionConfigs getInstance()
    {if(instance==null) {
        instance = new UdtExtensionConfigs();
        UdtExtensionConfig udtExtensionConfig = new UdtExtensionConfig();
        udtExtensionConfig.setExtensiontype("Form");
        udtExtensionConfig.setDeserclass("com.inspur.edp.web.designschema.udtextensiondef.FormUdtExtensionDeserializer");
        udtExtensionConfig.setSerclass("com.inspur.edp.web.designschema.udtextensiondef.FormUdtExtensionSerializer");
        instance.getExtensionConfigs().add(udtExtensionConfig);
    }
    return instance;
    }
    private List<UdtExtensionConfig> extensionConfigs=new ArrayList<>();

    public List<UdtExtensionConfig> getExtensionConfigs() {
        return extensionConfigs;
    }

    public void setExtensionConfigs(List<UdtExtensionConfig> extensionConfigs) {
        this.extensionConfigs = extensionConfigs;
    }

    public UdtExtensionConfig getExtensionConfig(String type)
    {
        if(type==null||type.equals(""))
            throw new RuntimeException();
        for(UdtExtensionConfig config:getExtensionConfigs())
        {
            if(config.getExtensiontype().equals(type))
                return config;
        }

        throw new RuntimeException();
    }
}
