/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.increment.extract.determination;

import com.inspur.edp.bef.bizentity.increment.entity.determination.DtmIncrement;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonDataTypeControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeControlRuleDef;
import com.inspur.edp.cef.designtime.api.increment.extractor.AbstractIncrementExtractor;

public class DtmIncrementExtractor extends AbstractIncrementExtractor {

  public DtmIncrement extractorIncrement(Determination oldAction, Determination newAction,
      CommonDataTypeControlRule rule, CommonDataTypeControlRuleDef def) {

    if (oldAction == null && newAction == null) {
      return null;
    } else if (oldAction == null) {
      return new AddedDtmExtractor().extract(newAction);
    } else if (newAction == null) {
      return null;
//            return (CommonEntityIncrement)(oldHelpConfig != null && newAction == null ? this.getDeletedEntityExtractor().extract(oldHelpConfig) : this.getModifyEntityExtractor().extract(oldHelpConfig, newAction, rule, def));
    } else {
//            ModifyMgrActionIncrement increment = new ModifyMgrActionIncrement();
//            increment.setActionId(newAction.getID());
//            increment.setAction(newAction);
//            return increment;
      return null;
    }
  }
}
