/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.parser.BeControlRuleDefParser;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.serializer.BeControlRuleDefSerializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmRuleNames;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;

@JsonSerialize(using = BeControlRuleDefSerializer.class)
@JsonDeserialize(using = BeControlRuleDefParser.class)
public class BeControlRuleDef extends CmControlRuleDef {

  public BeControlRuleDef() {
    super(null, BeControlRuleDefNames.beControlRuleObjectType);
    init();
  }


  private void init() {
    ControlRuleDefItem codeRule = new ControlRuleDefItem() {
      {
        this.setRuleName(CmRuleNames.Code);
        this.setRuleDisplayName("编号");
        this.setDescription("实体编号");
        this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
      }
    };
    setCodeControlRule(codeRule);

    ControlRuleDefItem nameRule = new ControlRuleDefItem() {
      {
        this.setRuleName(CmRuleNames.Name);
        this.setRuleDisplayName("名称");
        this.setDescription("实体名称");
        this.setDefaultRuleValue(ControlRuleValue.Allow);
      }
    };
    setNameControlRule(nameRule);

//        setUsingTimeStampControlRule();

    ControlRuleDefItem addActionRule = new ControlRuleDefItem() {
      {
        this.setRuleName(BeControlRuleDefNames.AddCustomAction);
        this.setRuleDisplayName("添加自定义动作");
        this.setDescription("添加自定义动作");
        this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
      }
    };
    setAddCustomActonControlRule(addActionRule);

    ControlRuleDefItem modifyActionRule = new ControlRuleDefItem() {
      {
        this.setRuleName(BeControlRuleDefNames.ModifyCustomActions);
        this.setRuleDisplayName("修改自定义动作");
        this.setDescription("修改自定义动作");
        this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
      }
    };
    setModifyCustomActonControlRule(modifyActionRule);

    ControlRuleDefItem addVarRule = new ControlRuleDefItem() {
      {
        this.setRuleName(BeControlRuleDefNames.AddVariableDtm);
        this.setRuleDisplayName("添加自定义变量");
        this.setDescription("添加自定义变量");
        this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
      }
    };
    setAddVariableDtmControlRule(addVarRule);

    ControlRuleDefItem modifyVarRule = new ControlRuleDefItem() {
      {
        this.setRuleName(BeControlRuleDefNames.ModifyVariableDtm);
        this.setRuleDisplayName("修改自定义变量");
        this.setDescription("修改自定义变量");
        this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
      }
    };
    setModifyVariableDtmControlRule(modifyVarRule);

    BeEntityControlRuleDef objectRuleDef = new BeEntityControlRuleDef(this);
    getChildControlRules().put(CommonModelNames.MainObject, objectRuleDef);

  }

  public ControlRuleDefItem getAddCustomActonControlRule() {
    return super.getControlRuleItem(BeControlRuleDefNames.AddCustomAction);
  }

  public void setAddCustomActonControlRule(ControlRuleDefItem ruleItem) {
    super.setControlRuleItem(BeControlRuleDefNames.AddCustomAction, ruleItem);
  }

  public ControlRuleDefItem getModifyCustomActonControlRule() {
    return super.getControlRuleItem(BeControlRuleDefNames.ModifyCustomActions);
  }

  public void setModifyCustomActonControlRule(ControlRuleDefItem ruleItem) {
    super.setControlRuleItem(BeControlRuleDefNames.ModifyCustomActions, ruleItem);
  }

  public ControlRuleDefItem getAddVariableDtmControlRule() {
    return super.getControlRuleItem(BeControlRuleDefNames.AddVariableDtm);
  }

  public void setAddVariableDtmControlRule(ControlRuleDefItem ruleItem) {
    super.setControlRuleItem(BeControlRuleDefNames.AddVariableDtm, ruleItem);
  }

  public ControlRuleDefItem getModifyVariableDtmControlRule() {
    return super.getControlRuleItem(BeControlRuleDefNames.ModifyVariableDtm);
  }

  public void setModifyVariableDtmControlRule(ControlRuleDefItem ruleItem) {
    super.setControlRuleItem(BeControlRuleDefNames.ModifyVariableDtm, ruleItem);
  }

  public ControlRuleDefItem getEnableTreeDtmControlRule() {
    return super.getControlRuleItem(BeControlRuleDefNames.EnableTreeDtm);
  }

  public void setEnableTreeDtmControlRule(ControlRuleDefItem ruleItem) {
    super.setControlRuleItem(BeControlRuleDefNames.EnableTreeDtm, ruleItem);
  }

  public ControlRuleDefItem getUsingTimeStampControlRule() {
    return super.getControlRuleItem(BeControlRuleDefNames.UsingTimeStamp);
  }

  public void setUsingTimeStampControlRule(ControlRuleDefItem ruleItem) {
    super.setControlRuleItem(BeControlRuleDefNames.UsingTimeStamp, ruleItem);
  }

}
