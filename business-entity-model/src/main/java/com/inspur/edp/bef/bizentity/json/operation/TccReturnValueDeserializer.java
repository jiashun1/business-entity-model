/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.bef.bizentity.operation.TccReturnValue;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;

public class TccReturnValueDeserializer extends BizParaDeserializer {

  @Override
  public final TccReturnValue deserialize(JsonParser jsonParser,
      DeserializationContext deserializationContext) {
    TccReturnValue rez = (TccReturnValue) super.deserialize(jsonParser, null);
    TccReturnValue newRez = new TccReturnValue();
    newRez.setID(rez.getID());
    newRez.setMode(rez.getMode());
    newRez.setAssembly(rez.getAssembly());
    newRez.setClassName(rez.getClassName());
    newRez.setCollectionParameterType(rez.getCollectionParameterType());
    return newRez;
  }

  @Override
  protected BizParameter createBizPara() {
    return new TccReturnValue();
  }
}
