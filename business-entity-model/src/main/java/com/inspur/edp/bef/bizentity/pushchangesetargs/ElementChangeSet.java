/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.pushchangesetargs;

public class ElementChangeSet {

  private ElementChangeDetail changeDetail;
  private ChangeType type;

  public ElementChangeSet() {
  }

  public ElementChangeSet(ElementChangeDetail changeDetail, ChangeType type) {
    this.changeDetail = changeDetail;
    this.type = type;
  }

  public ElementChangeDetail getChangeDetail() {
    return changeDetail;
  }

  public void setChangeDetail(ElementChangeDetail changeDetail) {
    this.changeDetail = changeDetail;
  }

  public ChangeType getType() {
    return type;
  }

  public void setType(ChangeType type) {
    this.type = type;
  }
}
