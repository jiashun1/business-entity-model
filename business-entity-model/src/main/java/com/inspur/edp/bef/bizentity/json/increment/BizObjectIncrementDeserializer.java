/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.increment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.increment.BizObjectIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.determination.DtmIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.validation.ValIncrement;
import com.inspur.edp.bef.bizentity.json.object.BizObjectDeserializer;
import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.das.commonmodel.json.increment.CommonElementIncrementDeserializer;
import com.inspur.edp.das.commonmodel.json.increment.CommonObjectIncrementDeserializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectDeserializer;
import java.io.IOException;

public class BizObjectIncrementDeserializer extends CommonObjectIncrementDeserializer {

  @Override
  protected CmObjectDeserializer getCommonObjectDeserializer() {
    return new BizObjectDeserializer();
  }

  @Override
  protected CommonElementIncrementDeserializer getCmElementIncrementDeserizlizer() {
    return new BizElementIncrementDeserializer();
  }

  @Override
  protected CommonObjectIncrementDeserializer getCmObjectIncrementDeserializer() {
    return new BizObjectIncrementDeserializer();
  }

  @Override
  protected ModifyEntityIncrement createModifyIncrement() {
    return new BizObjectIncrement();
  }

  @Override
  protected void readExtendModifyInfo(ModifyEntityIncrement value, JsonNode node) {

    BizObjectIncrement increment = (BizObjectIncrement) value;
    JsonNode dtmIncrements = node.get(BizEntityJsonConst.Determinations);
    if (dtmIncrements != null) {
      readDeterminations(increment, dtmIncrements);
    }

    JsonNode valIncrements = node.get(BizEntityJsonConst.Validations);
    if (valIncrements != null) {
      readValidations(increment, valIncrements);
    }
  }

  private void readDeterminations(BizObjectIncrement increment, JsonNode node) {
    ArrayNode array = trans2Array(node);
    for (JsonNode childNode : array) {
      DtmIncrementDeserializer serializer = new DtmIncrementDeserializer();
      ObjectMapper mapper = new ObjectMapper();
      SimpleModule module = new SimpleModule();
      module.addDeserializer(DtmIncrement.class, serializer);
      mapper.registerModule(module);
      try {
        String key = childNode.get(CefNames.Id).textValue();
        JsonNode valueNode = childNode.get(BizEntityJsonConst.Determination);

        DtmIncrement dtmIncrement = mapper
            .readValue(mapper.writeValueAsString(valueNode), DtmIncrement.class);
        dtmIncrement.setActionId(key);
        increment.getDeterminations().put(key, dtmIncrement);
      } catch (IOException e) {
        throw new RuntimeException("DtmIncrement反序列化失败", e);
      }
    }
  }

  private void readValidations(BizObjectIncrement increment, JsonNode node) {
    ArrayNode array = trans2Array(node);
    for (JsonNode childNode : array) {
      ValIncrementDeserializer serializer = new ValIncrementDeserializer();
      ObjectMapper mapper = new ObjectMapper();
      SimpleModule module = new SimpleModule();
      module.addDeserializer(ValIncrement.class, serializer);
      mapper.registerModule(module);
      try {
        String key = childNode.get(CefNames.Id).textValue();
        JsonNode valueNode = childNode.get(BizEntityJsonConst.Validation);

        ValIncrement valIncrement = mapper
            .readValue(mapper.writeValueAsString(valueNode), ValIncrement.class);
        valIncrement.setActionId(key);
        increment.getValidations().put(key, valIncrement);
      } catch (IOException e) {
        throw new RuntimeException("ValIncrement反序列化失败", e);
      }
    }
  }

  private ArrayNode trans2Array(JsonNode node) {
    try {
      return new ObjectMapper().treeToValue(node, ArrayNode.class);
    } catch (JsonProcessingException var3) {
      throw new RuntimeException("JsonNode转化失败，检查Json结构", var3);
    }
  }
}
