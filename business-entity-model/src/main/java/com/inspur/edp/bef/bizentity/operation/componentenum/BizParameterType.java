/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.operation.componentenum;

/**
 * 参数类型
 */
public enum BizParameterType {
  /**
   * 字符型
   */
  String,
  /**
   * 布尔型
   */
  Boolean,
  /**
   * 整数
   */
  Int32,
  /**
   * 浮点数字
   */
  Decimal,
  /**
   * 双浮点
   */
  Double,
  /**
   * 时间
   */
  DateTime,
  //对象类型
  Object,
  /**
   * 自定义
   */
  Custom;

  public int getValue() {
    return this.ordinal();
  }

  public static BizParameterType forValue(int value) {
    return values()[value];
  }
}
