/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.increment;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.increment.BizObjectIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.determination.DtmIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.validation.ValIncrement;
import com.inspur.edp.bef.bizentity.json.object.BizObjectSerializer;
import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.json.increment.CommonElementIncrementSerializer;
import com.inspur.edp.das.commonmodel.json.increment.CommonObjectIncrementSerializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectSerializer;
import java.io.IOException;
import java.util.HashMap;
import lombok.var;

public class BizObjectIncrementSerializer extends CommonObjectIncrementSerializer {

  @Override
  protected CmObjectSerializer getCommonObjectSerializer() {
    return new BizObjectSerializer();
  }

  @Override
  protected CommonElementIncrementSerializer getCmElementIncrementSerizlizer() {
    return new BizElementIncrementSerializer();
  }

  @Override
  protected CommonObjectIncrementSerializer getCmObjectIncrementSerializer() {
    return new BizObjectIncrementSerializer();
  }

  @Override
  protected void writeExtendModifyInfo(ModifyEntityIncrement value, JsonGenerator gen) {

    BizObjectIncrement increment = (BizObjectIncrement) value;
    writeDeterminations(increment.getDeterminations(), gen);
    writeValidation(increment.getValidations(), gen);
  }

  private void writeDeterminations(HashMap<String, DtmIncrement> actions, JsonGenerator gen) {
    if (actions == null) {
      return;
    }
    SerializerUtils.writePropertyName(gen, BizEntityJsonConst.Determinations);
    SerializerUtils.WriteStartArray(gen);
    for (var item : actions.entrySet()) {
      try {
        SerializerUtils.writeStartObject(gen);
        SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

        SerializerUtils.writePropertyName(gen, BizEntityJsonConst.Determination);
        DtmIncrementSerializer serializer = new DtmIncrementSerializer();
        serializer.serialize(item.getValue(), gen, null);
        SerializerUtils.writeEndObject(gen);
      } catch (IOException e) {
        throw new RuntimeException("联动计算增量序列化失败", e);
      }
    }
    SerializerUtils.WriteEndArray(gen);
  }


  private void writeValidation(HashMap<String, ValIncrement> actions, JsonGenerator gen) {
    if (actions == null) {
      return;
    }
    SerializerUtils.writePropertyName(gen, BizEntityJsonConst.Validations);
    SerializerUtils.WriteStartArray(gen);
    for (var item : actions.entrySet()) {
      try {
        SerializerUtils.writeStartObject(gen);
        SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

        SerializerUtils.writePropertyName(gen, BizEntityJsonConst.Validation);
        ValIncrementSerializer serializer = new ValIncrementSerializer();
        serializer.serialize(item.getValue(), gen, null);
        SerializerUtils.writeEndObject(gen);
      } catch (IOException e) {
        throw new RuntimeException("校验规则增量序列化失败", e);
      }
    }
    SerializerUtils.WriteEndArray(gen);
  }
}
