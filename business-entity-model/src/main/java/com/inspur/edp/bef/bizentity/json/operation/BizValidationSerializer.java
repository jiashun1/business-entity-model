/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.bef.bizentity.beenum.BEValidationType;
import com.inspur.edp.bef.bizentity.beenum.RequestNodeTriggerType;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.cef.designtime.api.collection.ValElementCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import lombok.var;

public class BizValidationSerializer extends BizOperationSerializer<Validation> {

  public BizValidationSerializer() {
  }

  public BizValidationSerializer(boolean full) {
    super(full);
    isFull = full;
  }

  @Override
  protected void writeExtendOperationBaseProperty(JsonGenerator writer, BizOperation op) {

  }

  @Override
  protected void writeExtendOperationSelfProperty(JsonGenerator writer, BizOperation op) {
    Validation validation = (Validation) op;
    if (isFull || validation.getValidationType() != BEValidationType.Consistency) {
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ValidationType,
          validation.getValidationType().getValue());
    }
    int triggerTimePointType = 0;
    for (BETriggerTimePointType timePointType : validation.getTriggerTimePointType()) {
      triggerTimePointType += timePointType.getValue();
    }
    SerializerUtils
        .writePropertyValue(writer, BizEntityJsonConst.TriggerTimePointType, triggerTimePointType);
//        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.Order, validation.getOrder());
//        WritePrecedingIds(writer, validation);
//        WriteSucceedingIds(writer, validation);
    int requestNodeTriggerType = 0;
    for (RequestNodeTriggerType requestNodeTrigger : validation.getRequestNodeTriggerType()) {
      requestNodeTriggerType += requestNodeTrigger.getValue();
    }
    SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.RequestNodeTriggerType,
        requestNodeTriggerType);

    writeParameters(writer, validation);
    writeRequestElements(writer, validation);
    writeRequestChildElements(writer, validation);
    writeValidationTriggerPoints(writer, validation);
  }

//    private void WritePrecedingIds(JsonGenerator writer, Validation validation)
//    {
//        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.PrecedingIds);
//        var convertor = new ListJsonConvertor<String>();
//        convertor.WriteJson(writer, validation.getPrecedingIds(), null);
//    }

//    private void WriteSucceedingIds(JsonGenerator writer, Validation validation)
//    {
//        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.SucceedingIds);
//        var convertor = new ListJsonConvertor<string>();
//        convertor.WriteJson(writer, validation.getSucceedingIds(), null);
//    }

  private void writeRequestElements(JsonGenerator writer, Validation dtm) {
    if (!isFull && (dtm.getRequestElements() == null || dtm.getRequestElements().size() == 0)) {
      return;
    }
    SerializerUtils.writePropertyName(writer, BizEntityJsonConst.RequestElements);
    writeValidationElementCollection(writer, dtm.getRequestElements());
  }

  private void writeValidationElementCollection(JsonGenerator writer,
      ValElementCollection childElementsIds) {
    //[
    SerializerUtils.WriteStartArray(writer);
    if (childElementsIds.size() > 0) {
      for (String item : childElementsIds) {
        SerializerUtils.writePropertyValue_String(writer, item);
      }
    }
    //]
    SerializerUtils.WriteEndArray(writer);
  }

  private void writeRequestChildElements(JsonGenerator writer, Validation dtm) {
    HashMap<String, ValElementCollection> dic = dtm.getRequestChildElements();
    if (isFull || (dic != null && dic.size() > 0)) {
      SerializerUtils.writePropertyName(writer, BizEntityJsonConst.RequestChildElements);
      SerializerUtils.WriteStartArray(writer);
      for (Map.Entry<String, ValElementCollection> item : dic.entrySet()) {
        SerializerUtils.writeStartObject(writer);
        SerializerUtils
            .writePropertyValue(writer, BizEntityJsonConst.RequestChildElementKey, item.getKey());
        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.RequestChildElementValue);
        writeValidationElementCollection(writer, item.getValue());
        SerializerUtils.writeEndObject(writer);
      }
      SerializerUtils.WriteEndArray(writer);
    }
  }

  private void writeValidationTriggerPoints(JsonGenerator writer, Validation val) {
    var dic = val.getValidationTriggerPoints();
    if (isFull || (dic != null && dic.size() > 0)) {
      SerializerUtils.writePropertyName(writer, BizEntityJsonConst.ValidationTriggerPoints);
      SerializerUtils.WriteStartArray(writer);
      for (Map.Entry<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> item : dic
          .entrySet()) {
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ValidationTriggerPointKey,
            item.getKey().getValue());
        int requestNodeTriggerType = 0;
        for (var trigger : item.getValue()) {
          requestNodeTriggerType += trigger.getValue();
        }
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ValidationTriggerPointValue,
            requestNodeTriggerType);
        SerializerUtils.writeEndObject(writer);
      }
      SerializerUtils.WriteEndArray(writer);
    }
  }

  private void writeParameters(JsonGenerator writer, Validation validation) {
    if (isFull || (validation.getParameterCollection() != null
        && validation.getParameterCollection().getCount() > 0)) {
      SerializerUtils.writePropertyName(writer, BizEntityJsonConst.Parameters);
      //[
      SerializerUtils.WriteStartArray(writer);
      if (validation.getParameterCollection() != null
          && validation.getParameterCollection().getCount() > 0) {
        for (Object item : validation.getParameterCollection()) {
          new BizParameterSerializer(isFull).serialize((BizParameter) item, writer, null);
        }
      }
      //]
      SerializerUtils.WriteEndArray(writer);
    }
  }
}
