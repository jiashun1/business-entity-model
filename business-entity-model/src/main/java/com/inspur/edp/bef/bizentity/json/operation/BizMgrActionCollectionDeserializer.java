/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.inspur.edp.bef.bizentity.common.InternalActionUtil;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.collection.BizMgrActionCollection;

public class BizMgrActionCollectionDeserializer extends
    BizOperationCollectionDeserializer<BizMgrActionCollection> {

  @Override
  protected BizMgrActionCollection createCollection() {
    return new BizMgrActionCollection();
  }

  @Override
  protected BizMgrActionDeserializer createDeserializer() {
    return new BizMgrActionDeserializer();
  }

  @Override
  protected void addBizOp(BizMgrActionCollection collection, BizOperation op) {
    if (!InternalActionUtil.InternalMgrActionIDs.contains(op.getID())) {
      super.addBizOp(collection, op);
    }
  }
}
