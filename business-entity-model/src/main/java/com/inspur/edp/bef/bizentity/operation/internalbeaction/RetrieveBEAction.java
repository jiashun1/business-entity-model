/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.operation.internalbeaction;

import com.inspur.edp.bef.bizentity.operation.BizAction;

/**
 * 内置Retrieve操作
 */
public class RetrieveBEAction extends BizAction implements IInternalBEAction {

  public static final String id = "cJwzDlFfa0uz4rChlTEd4g";
  public static final String code = "Retrieve";
  public static final String name = "内置检索操作";

  public RetrieveBEAction() {
    setID(id);
    setCode(code);
    setName(name);
  }
}
