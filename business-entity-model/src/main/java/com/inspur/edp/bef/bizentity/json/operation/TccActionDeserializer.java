/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;


import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.TccAction;
import com.inspur.edp.bef.bizentity.operation.TccReturnValue;

public class TccActionDeserializer extends BizOperationDeserializer<TccAction> {

  @Override
  protected TccAction createBizOp() {
    return new TccAction();
  }

  @Override
  protected void beforeDeserializeBizoperation(BizOperation op) {

  }

  @Override
  protected final boolean readExtendOpProperty(BizOperation op, String propName,
      JsonParser jsonParser) {
    TccAction action = (TccAction) op;
    boolean hasProperty = true;
    switch (propName) {
      case BizEntityJsonConst.ReturnValue:
        readReturnValue(jsonParser, action);
        break;
      default:
        hasProperty = false;
        break;
    }
    return hasProperty;
  }

  private void readReturnValue(JsonParser jsonParser, TccAction action) {
    TccReturnValueDeserializer returnValueDeserializer = new TccReturnValueDeserializer();
    TccReturnValue value = returnValueDeserializer.deserialize(jsonParser, null);
    action.setTccReturnValue(value);
  }

}
