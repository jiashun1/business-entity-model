/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.controlrule.rule;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.bef.bizentity.controlrule.rule.parser.BeControlRuleParser;
import com.inspur.edp.bef.bizentity.controlrule.rule.serializer.BeRuleSerializer;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity.BeControlRuleDefNames;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;
import com.inspur.edp.das.commonmodel.controlrule.CmEntityControlRule;

@JsonSerialize(using = BeRuleSerializer.class)
@JsonDeserialize(using = BeControlRuleParser.class)
public class BeControlRule extends CmControlRule {

  protected final CmEntityControlRule createMainEntityControlRule() {
    return new BeObjControlRule();
  }

  public ControlRuleItem getAddCustomActonControlRule() {
    return super.getControlRule(BeControlRuleDefNames.AddCustomAction);
  }

  public void setAddCustomActonControlRule(ControlRuleItem ruleItem) {
    super.setControlRule(BeControlRuleDefNames.AddCustomAction, ruleItem);
  }

  public ControlRuleItem getAddVariableControlRule() {
    return super.getControlRule(BeControlRuleDefNames.AddVariableDtm);
  }

  public void setAddVariableControlRule(ControlRuleItem ruleItem) {
    super.setControlRule(BeControlRuleDefNames.AddVariableDtm, ruleItem);
  }

  public ControlRuleItem getEnableTreeDtmControlRule() {
    return super.getControlRule(BeControlRuleDefNames.EnableTreeDtm);
  }

  public void setEnableTreeDtmControlRule(ControlRuleItem ruleItem) {
    super.setControlRule(BeControlRuleDefNames.EnableTreeDtm, ruleItem);
  }

  public ControlRuleItem getUsingTimeStampControlRule() {
    return super.getControlRule(BeControlRuleDefNames.UsingTimeStamp);
  }

  public void setUsingTimeStampControlRule(ControlRuleItem ruleItem) {
    super.setControlRule(BeControlRuleDefNames.UsingTimeStamp, ruleItem);
  }
}
