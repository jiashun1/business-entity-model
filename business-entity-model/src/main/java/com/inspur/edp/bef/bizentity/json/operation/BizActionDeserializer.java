/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.bef.bizentity.operation.BizAction;
import com.inspur.edp.bef.bizentity.operation.BizActionBase;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.beomponent.BizActionParamCollection;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;

public class BizActionDeserializer extends BizActionBaseDeserializer {

  @Override
  protected final boolean readExtendActionProperty(JsonParser jsonParser, BizOperation op,
      String propName) {
    return false;
  }

  @Override
  protected BizActionBase createBizActionBase() {
    return new BizAction();
  }

  @Override
  protected BizParaDeserializer createPrapConvertor() {
    return new BizActionParaDeserializer();
  }

  @Override
  protected BizParameterCollection createPrapCollection() {
    return new BizActionParamCollection();
  }
}
