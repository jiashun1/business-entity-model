/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.operation;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.bef.bizentity.beenum.BEOperationType;
import com.inspur.edp.bef.bizentity.json.operation.BizMgrActionDeserializer;
import com.inspur.edp.bef.bizentity.json.operation.BizMgrActionSerializer;
import com.inspur.edp.bef.bizentity.operation.bemgrcomponent.BizMgrActionParamCollection;

/**
 * BEMgr操作
 */
@JsonSerialize(using = BizMgrActionSerializer.class)
@JsonDeserialize(using = BizMgrActionDeserializer.class)
public class BizMgrAction extends BizActionBase implements Cloneable {

  /**
   * 业务操作ID
   */
  private String privateFuncOperationID;

  public final String getFuncOperationID() {
    return privateFuncOperationID;
  }

  public final void setFuncOperationID(String value) {
    privateFuncOperationID = value;
  }

  /**
   * 业务操作名称, 显示用
   */
  private String privateFuncOperationName;

  public final String getFuncOperationName() {
    return privateFuncOperationName;
  }

  public final void setFuncOperationName(String value) {
    privateFuncOperationName = value;
  }

  private BizMgrActionParamCollection beMgrParams;

  /**
   * 操作类型
   */
  @Override
  public BEOperationType getOpType() {
    return BEOperationType.BizMgrAction;
  }

  public BizMgrAction() {
    super();
    beMgrParams = new BizMgrActionParamCollection();
  }

  @Override
  protected BizMgrActionParamCollection getActionParameters() {
    return beMgrParams;
  }
}
