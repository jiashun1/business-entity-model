/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.gspbusinessentity.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "GSPBusinessEntity")
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class GspBeExtendInfoEntity {

  @Id
  private String id;
  private String extendInfo;
  private String configId;

  private String createdBy;
  private Date createdOn;
  private String lastChangedBy;
  private Date lastChangedOn;

  private String beConfigCollectionInfo;

  public GspBeExtendInfoEntity(String id, String extendInfo, String configId, String createdBy,
      Date createdOn,
      String lastChangedBy, Date lastChangedOn) {
    this.id = id;
    this.extendInfo = extendInfo;
    this.configId = configId;
    this.createdBy = createdBy;
    this.createdOn = createdOn;
    this.lastChangedBy = lastChangedBy;
    this.lastChangedOn = lastChangedOn;
  }

  public GspBeExtendInfoEntity(String id, String extendInfo, String configId, String createdBy,
      Date createdOn,
      String lastChangedBy, Date lastChangedOn, String beConfigCollectionInfo) {
    this.id = id;
    this.extendInfo = extendInfo;
    this.configId = configId;
    this.createdBy = createdBy;
    this.createdOn = createdOn;
    this.lastChangedBy = lastChangedBy;
    this.lastChangedOn = lastChangedOn;
    this.beConfigCollectionInfo = beConfigCollectionInfo;
  }


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getExtendInfo() {
    return extendInfo;
  }

  public void setExtendInfo(String extendInfo) {
    this.extendInfo = extendInfo;
  }

  public String getConfigId() {
    return configId;
  }

  public void setConfigId(String configId) {
    this.configId = configId;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public String getLastChangedBy() {
    return lastChangedBy;
  }

  public void setLastChangedBy(String lastChangedBy) {
    this.lastChangedBy = lastChangedBy;
  }

  public Date getLastChangedOn() {
    return lastChangedOn;
  }

  public void setLastChangedOn(Date lastChangedOn) {
    this.lastChangedOn = lastChangedOn;
  }

  public String getBeConfigCollectionInfo() {
    return beConfigCollectionInfo;
  }

  public void setBeConfigCollectionInfo(
      String beConfigCollectionInfo) {
    this.beConfigCollectionInfo = beConfigCollectionInfo;
  }


}
