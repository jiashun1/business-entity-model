/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.operation;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.beenum.BEOperationType;
import com.inspur.edp.cef.designtime.api.entity.CustomizationInfo;
import com.inspur.edp.cef.designtime.api.util.Guid;
import org.springframework.util.ObjectUtils;

/**
 * BE操作基类. BE操作，是基于服务端业务逻辑构件的，每个操作都是其中的一个方法。 但BE对用户隐藏了该层关系，即让用户感觉不到后台构件的存在。
 */
public abstract class BizOperation implements Cloneable {

  /**
   * 主键
   */
  private String privateID;

  public final String getID() {
    return privateID;
  }

  public final void setID(String value) {
    privateID = value;
  }

  /**
   * 编号
   */
  private String privateCode;

  public final String getCode() {
    return privateCode;
  }

  public final void setCode(String value) {
    privateCode = value;
  }

  /**
   * 显示名称
   */
  private String privateName;

  public final String getName() {
    return privateName;
  }

  public final void setName(String value) {
    privateName = value;
  }

  /**
   * 描述
   */
  private String privateDescription;

  public final String getDescription() {
    return privateDescription;
  }

  public final void setDescription(String value) {
    privateDescription = value;
  }

  /**
   * 对应构件实体Id
   */
  private String privateComponentId;

  public String getComponentId() {
    return privateComponentId;
  }

  public void setComponentId(String value) {
    privateComponentId = value;
  }

  /**
   * 对应构件实体Name
   */
  private String privateComponentName;

  public String getComponentName() {
    return privateComponentName;
  }

  public void setComponentName(String value) {
    privateComponentName = value;
  }

  /**
   * 对应构件实体元数据包名
   */
  private String privateComponentPkgName;

  public String getComponentPkgName() {
    return privateComponentPkgName;
  }

  public void setComponentPkgName(String value) {
    privateComponentPkgName = value;
  }

  /**
   * 是否可见
   */
  private boolean privateIsVisible;

  public final boolean getIsVisible() {
    return privateIsVisible;
  }

  public final void setIsVisible(boolean value) {
    privateIsVisible = value;
  }

  /**
   * 操作类型
   */
  public abstract BEOperationType getOpType();

  /**
   * 所属的节点
   */
  private GspBizEntityObject privateOwner;

  public final GspBizEntityObject getOwner() {
    return privateOwner;
  }

  public final void setOwner(GspBizEntityObject value) {
    privateOwner = value;
  }

  /**
   * 所属扩展模型元数据ID
   */
  private String privateBelongModelID;

  public final String getBelongModelID() {
    return privateBelongModelID;
  }

  public final void setBelongModelID(String value) {
    privateBelongModelID = value;
  }

  private boolean privateIsRef;

  public final boolean getIsRef() {
    return privateIsRef;
  }

  public final void setIsRef(boolean value) {
    privateIsRef = value;
  }

  private boolean privateIsGenerateComponent;

  public final boolean getIsGenerateComponent() {
    return privateIsGenerateComponent;
  }

  public final void setIsGenerateComponent(boolean value) {
    privateIsGenerateComponent = value;
  }

  private CustomizationInfo customizationInfo = new CustomizationInfo();

  public CustomizationInfo getCustomizationInfo() {
    return this.customizationInfo;
  }

  public void setCustomizationInfo(CustomizationInfo customizationInfo) {
    this.customizationInfo = customizationInfo;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }
    return equals((BizOperation) obj);
  }

  protected boolean equals(BizOperation other) {
    return getID().equals(other.getID()) && getCode().equals(other.getCode()) && getName()
        .equals(other.getName())
        && ObjectUtils.nullSafeEquals(getComponentId(), other.getComponentId()) && (new Boolean(
        getIsVisible())).equals(other.getIsVisible())
        && getOpType() == other.getOpType() && (new Boolean(getIsRef())).equals(other.getIsRef());
  }

  @Override
  public int hashCode() {
    {
      int hashCode = (getID() != null ? getID().hashCode() : 0);
      hashCode = (hashCode * 397) ^ (getCode() != null ? getCode().hashCode() : 0);
      hashCode = (hashCode * 397) ^ (getName() != null ? getName().hashCode() : 0);
      hashCode = (hashCode * 397) ^ (getComponentId() != null ? getComponentId().hashCode() : 0);
      hashCode = (hashCode * 397) ^ (new Boolean(getIsVisible())).hashCode();
      hashCode = (hashCode * 397) ^ (int) getOpType().getValue();
      hashCode = (hashCode * 397) ^ (new Boolean(getIsRef())).hashCode();
      return hashCode;
    }
  }

  public BizOperation clone(boolean isGenerateId) {
    Object tempVar = null;
    try {
      tempVar = super.clone();
    } catch (CloneNotSupportedException e) {
      return null;
    }
    BizOperation result = (BizOperation) ((tempVar instanceof BizOperation) ? tempVar : null);

    if (isGenerateId) {
      result.setID(Guid.newGuid().toString());
      result.setCode("Copyof" + result.getCode());
    }

    return result;
  }

  public BizOperation clone() {
    return clone(false);
  }

  @Override
  public String toString() {
    return String
        .format("Id:%1$s,Code:%2$s,Name:%3$s,ComponentId:%4$s", getID(), getCode(), getName(),
            getComponentId());
  }

  protected final void mergeOperationBaseInfo(BizOperation dependentOperation) {
    setCode(dependentOperation.getCode());
    setName(dependentOperation.getName());
    setDescription(dependentOperation.getDescription());
    // TODO:method��merge
    setComponentId(dependentOperation.getComponentId());
    setBelongModelID(dependentOperation.getBelongModelID());
    setIsVisible(dependentOperation.getIsVisible());
  }

}
