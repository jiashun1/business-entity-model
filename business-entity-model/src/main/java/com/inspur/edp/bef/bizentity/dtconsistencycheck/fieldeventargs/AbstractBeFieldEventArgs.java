/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs;


import com.inspur.edp.cef.designtime.api.dtconsistencycheck.AbstractDtEventArgs;

public abstract class AbstractBeFieldEventArgs extends AbstractDtEventArgs {

  protected String beId;
  protected String entityId;
  protected String fieldId;
  protected String metadataPath;

  public AbstractBeFieldEventArgs() {

  }

  public AbstractBeFieldEventArgs(String beId, String entityId, String fieldId) {
    this.beId = beId;
    this.entityId = entityId;
    this.fieldId = fieldId;
  }

  public void setMetadataPath(String metadataPath) {
    this.metadataPath = metadataPath;
  }

  public String getMetadataPath() {
    return metadataPath;
  }

  public String getBeId() {
    return beId;
  }

  public void setBeId(String beId) {
    this.beId = beId;
  }

  public String getEntityId() {
    return entityId;
  }

  public void setEntityId(String entityId) {
    this.entityId = entityId;
  }

  public String getFieldId() {
    return fieldId;
  }

  public void setFieldId(String fieldId) {
    this.fieldId = fieldId;
  }
}
