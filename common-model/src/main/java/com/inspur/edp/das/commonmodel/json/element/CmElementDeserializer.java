/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.cef.designtime.api.collection.GspFieldCollection;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.cef.designtime.api.json.element.GspAssociationDeserializer;
import com.inspur.edp.das.commonmodel.collection.GspElementCollection;
import com.inspur.edp.das.commonmodel.entity.GspCommonElement;
import com.inspur.edp.das.commonmodel.entity.element.ElementCodeRuleConfig;
import com.inspur.edp.das.commonmodel.entity.element.GspBillCodeGenerateOccasion;
import com.inspur.edp.das.commonmodel.entity.element.GspBillCodeGenerateType;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldDeserializer;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;
/**
 * The Json Parser Of  CmELement
 *
 * @ClassName: VersionControlInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class CmElementDeserializer extends CefFieldDeserializer {

	protected abstract void beforeCMElementDeserializer(GspCommonElement field);

    @Override
	protected final void beforeCefElementDeserializer(GspCommonField item){
		GspCommonElement field = (GspCommonElement) item;
		beforeCMElementDeserializer(field);
    	field.setIsCustomItem(false);
    }

	@Override
	protected final GspCommonField CreateField() {
		return createElement();
	}



	@Override
	protected final boolean readExtendFieldProperty(GspCommonField item, String propName, JsonParser jsonParser) {
		boolean result = true;
		GspCommonElement field = (GspCommonElement) item;
		switch (propName) {
			case CommonModelNames.BillCodeConfig:
				field.setBillCodeConfig(readBillCodeConfig(jsonParser));
				break;
			case CommonModelNames.ColumnID:
				field.setColumnID(SerializerUtils.readPropertyValue_String(jsonParser));
				break;
			case CommonModelNames.Readonly:
				field.setReadonly(SerializerUtils.readPropertyValue_boolean(jsonParser));
				break;
			case CommonModelNames.IsCustomItem:
				field.setIsCustomItem(SerializerUtils.readPropertyValue_boolean(jsonParser));
				break;
			case CommonModelNames.BelongModelID:
				field.setBelongModelID(SerializerUtils.readPropertyValue_String(jsonParser));
				break;
			default:
				result = readExtendElementProperty(field, propName, jsonParser);
				break;
		}
		return result;
	}

	protected abstract GspCommonElement createElement();

	@Override
	protected GspFieldCollection CreateFieldCollection() {
		return new GspElementCollection(null);
	}

	protected boolean readExtendElementProperty(GspCommonElement field, String propName, JsonParser jsonParser) {
		return false;
	}


	private ElementCodeRuleConfig readBillCodeConfig(JsonParser jsonParser) {
		ElementCodeRuleConfig elementCodeRuleConfig = new ElementCodeRuleConfig();
		if (SerializerUtils.readNullObject(jsonParser)) {
			return elementCodeRuleConfig;
		}
        elementCodeRuleConfig.setCanBillCode(false);
        elementCodeRuleConfig.setBillCodeID("");
        elementCodeRuleConfig.setBillCodeName("");
        elementCodeRuleConfig.setCodeGenerateOccasion(GspBillCodeGenerateOccasion.SystemProcess);
        elementCodeRuleConfig.setCodeGenerateType(GspBillCodeGenerateType.none);
		SerializerUtils.readStartObject(jsonParser);
		while (jsonParser.getCurrentToken() == FIELD_NAME) {
			String propName = SerializerUtils.readPropertyName(jsonParser);
			readBillCodeConfigPropertyValue(elementCodeRuleConfig, propName, jsonParser);
		}
		SerializerUtils.readEndObject(jsonParser);
		return elementCodeRuleConfig;
	}

	private void readBillCodeConfigPropertyValue(ElementCodeRuleConfig elementCodeRuleConfig, String propName, JsonParser jsonParser) {
		switch (propName) {
			case CommonModelNames.CanBillCode:
				elementCodeRuleConfig.setCanBillCode(SerializerUtils.readPropertyValue_boolean(jsonParser));
				break;
			case CommonModelNames.BillCodeID:
				elementCodeRuleConfig.setBillCodeID(SerializerUtils.readPropertyValue_String(jsonParser));
				break;
			case CommonModelNames.BillCodeName:
				elementCodeRuleConfig.setBillCodeName(SerializerUtils.readPropertyValue_String(jsonParser));
				break;
			case CommonModelNames.CodeGenerateOccasion:
				elementCodeRuleConfig.setCodeGenerateOccasion(SerializerUtils.readPropertyValue_Enum(jsonParser, GspBillCodeGenerateOccasion.class, GspBillCodeGenerateOccasion.values()));
				break;
			case CommonModelNames.CodeGenerateType:
				elementCodeRuleConfig.setCodeGenerateType(SerializerUtils.readPropertyValue_Enum(jsonParser, GspBillCodeGenerateType.class, GspBillCodeGenerateType.values()));
				break;
			default:
				throw new RuntimeException(String.format("%s%s%s", "MappingInfoDeserializer未识别的属性名：", propName, "。请将系统补丁升级至可更新范围内最新。"));
		}
	}

	@Override
	protected GspAssociationDeserializer CreateGspAssoDeserializer() {
		return new GspCommonAssociationDeserializer(this);
	}
}
