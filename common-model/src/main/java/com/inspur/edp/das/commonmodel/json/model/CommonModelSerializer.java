/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.json.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.Variable.CommonVariableEntitySeriazlier;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import com.inspur.edp.das.commonmodel.json.object.CmObjectSerializer;
import org.springframework.util.StringUtils;

/**
 * The Json Serializer Of Common Model
 *
 * @ClassName: CommonModelSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class CommonModelSerializer extends JsonSerializer<IGspCommonModel> {

    protected boolean isFull = true;

    public CommonModelSerializer(boolean full){
        isFull = full;
    }
    public CommonModelSerializer(){};

    @Override
    public void serialize(IGspCommonModel model, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {

        SerializerUtils.writeStartObject(jsonGenerator);
        writeBaseProperty(model, jsonGenerator);
        writeSelfProperty(jsonGenerator, model);
        SerializerUtils.writeEndObject(jsonGenerator);
    }

    //region BaseSelf

    private void writeBaseProperty(IGspCommonModel model,JsonGenerator writer) {
        writeModelInfo(model, writer);
        writeExtendModelProperty(model, writer);
    }

    private void writeModelInfo(IGspCommonModel model, JsonGenerator jsonGenerator) {
        writeBaseInfo(model, jsonGenerator);
        writeCommonModelMainObjectInfo(model,jsonGenerator);
        writeVariableEntity(model,jsonGenerator);
    }

    private void writeBaseInfo(IGspCommonModel model, JsonGenerator jsonGenerator) {
        SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.ID, model.getID());
        SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.Code, model.getCode());
        SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.Name, model.getName());
        if(isFull || !StringUtils.isEmpty(model.getDescription())){
            SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.DESCRIPTION, model.getDescription());
        }
        if(isFull){
            SerializerUtils.writePropertyValue(jsonGenerator, CefNames.I18nResourceInfoPrefix, model.getI18nResourceInfoPrefix());}
        else if(model.getI18nResourceInfoPrefix()!=null&&!"".equals(model.getI18nResourceInfoPrefix())) {
            if(!model.getI18nResourceInfoPrefix().equals(model.getDotnetGeneratingAssembly()+"."+model.getCode()))
                SerializerUtils.writePropertyValue(jsonGenerator, CefNames.I18nResourceInfoPrefix, model.getI18nResourceInfoPrefix());}
        else {
            SerializerUtils.writePropertyValue(jsonGenerator, CefNames.I18nResourceInfoPrefix, model.getI18nResourceInfoPrefix());}
        if(isFull||model.getIsVirtual())
            SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.IsVirtual, model.getIsVirtual());
        if(isFull||(model.getEntityType()!=null&&!"".equals(model.getEntityType())))
            SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.EntityType, model.getEntityType());
        SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.GeneratingAssembly, model.getDotnetGeneratingAssembly());//不变
        SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.IsUseNamespaceConfig, model.getIsUseNamespaceConfig());
        if(isFull||((model.getVersionContronInfo()!=null)&&(model.getVersionContronInfo().getVersionControlElementId()!=null&&!"".equals(model.getVersionContronInfo().getVersionControlElementId()))))
            SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.VersionControlInfo, model.getVersionContronInfo());
        if(isFull||(model.getBeLabel()!=null&&model.getBeLabel().size()>0))
            SerializerUtils.writePropertyValue(jsonGenerator,CommonModelNames.BeLabel,model.getBeLabel());
        if(isFull||model.isSimplifyGen())
            SerializerUtils.writePropertyValue(jsonGenerator,CommonModelNames.IsSimplifyGen,model.isSimplifyGen());
        if(isFull || model.isSimpBeanConfig()){
            SerializerUtils.writePropertyValue(jsonGenerator,CommonModelNames.IsSimpBeanConfig,model.isSimpBeanConfig());
        }

        writeFkConstraints(model, jsonGenerator);
    }

    private void writeFkConstraints(IGspCommonModel model, JsonGenerator jsonGenerator) {
//        if (model.getFkConstraints() == null || model.FkConstraints.Count <= 0)
//            return;
//        XmlElement mhElement = AddElement(parent, CommonModelNames.FkConstraints);
//        foreach (RelationForeignKeyConstraint constraint in model.FkConstraints)
//        {
//            XmlElement constraintElement = AddElement(mhElement, CommonModelNames.FkConstraint);
//            constraintElement.SetAttribute(CommonModelNames.Value, Ecp.Caf.Common.Serializer.Serialize(constraint));
//        }
    }

    private void writeCommonModelMainObjectInfo(IGspCommonModel model, JsonGenerator jsonGenerator) {
        if (isFull||model.getMainObject() != null) {
            SerializerUtils.writePropertyName(jsonGenerator, CommonModelNames.MainObject);
            //IGspCommonObject
            CmObjectSerializer cmObjectSerializer = getCmObjectSerializer();
            if(model instanceof GspCommonModel){
                cmObjectSerializer.setFlag(((GspCommonModel)model).getFlag());
            }
            cmObjectSerializer.serialize(model.getMainObject(), jsonGenerator, null);
        }
    }

    private void writeVariableEntity(IGspCommonModel model, JsonGenerator writer) {
        if (isFull||model.getVariables() != null) {
            SerializerUtils.writePropertyName(writer, CommonModelNames.Variables);
            (new CommonVariableEntitySeriazlier(isFull)).serialize(model.getVariables(), writer, null);
        }
    }
    //endregion

    //region SelfProp
    private void writeSelfProperty(JsonGenerator writer, IGspCommonModel model) {
        //扩展模型属性
        writeExtendModelSelfProperty(model, writer);
    }
    //endregion

    protected abstract void writeExtendModelProperty(IGspCommonModel model, JsonGenerator jsonGenerator);
    protected abstract void writeExtendModelSelfProperty(IGspCommonModel model,JsonGenerator writer);
    protected abstract CmObjectSerializer getCmObjectSerializer();
}
