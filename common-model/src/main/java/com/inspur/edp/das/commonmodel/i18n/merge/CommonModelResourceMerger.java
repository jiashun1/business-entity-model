/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.i18n.merge;

import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.cef.designtime.api.i18n.merger.AbstractResourceMerger;
import com.inspur.edp.cef.designtime.api.i18n.names.CefResourceKeyNames;
import com.inspur.edp.cef.designtime.api.increment.merger.MergeUtils;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;

public abstract class CommonModelResourceMerger extends AbstractResourceMerger {
    private IGspCommonModel model;
    protected CommonModelResourceMerger(IGspCommonModel model,ICefResourceMergeContext context) {
        super(context);
        this.model=model;
    }

    @Override
    protected void mergeItems() {
        extractMainObject(model);
    }
    private void extractMainObject(IGspCommonModel model) {
        IGspCommonObject mainObj = model.getMainObject();
        I18nResourceItemCollection resourceItems=getContext().getResourceItems();
        String keyPrefix= MergeUtils.getKeyPrefix(model.getI18nResourceInfoPrefix(), CefResourceKeyNames.Name);
        model.setName(resourceItems.getResourceItemByKey(keyPrefix).getValue());
        getObjectResourceMerger(getContext(), mainObj).merge();
    }
    //[业务实体]、[视图对象]
    protected abstract String getModelDescriptionName();

    protected abstract CommonobjectResourceMerger getObjectResourceMerger(ICefResourceMergeContext context,  IGspCommonObject obj);

    protected abstract void extractExtendProperties(IGspCommonModel model);
}
