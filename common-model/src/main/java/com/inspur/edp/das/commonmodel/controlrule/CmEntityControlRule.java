/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.controlrule;

import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonDataTypeControlRule;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityRuleNames;

public class CmEntityControlRule extends CommonDataTypeControlRule {

    public ControlRuleItem getAddChildEntityControlRule() {
        return super.getControlRule(CmEntityRuleNames.AddChildEntity);
    }

    public void setAddChildEntityControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CmEntityRuleNames.AddChildEntity, ruleItem);
    }

    public ControlRuleItem getModifyChildEntitiesControlRule() {
        return super.getControlRule(CmEntityRuleNames.ModifyChildEntities);
    }

    public void setModifyChildEntitiesControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CmEntityRuleNames.ModifyChildEntities, ruleItem);
    }

}
