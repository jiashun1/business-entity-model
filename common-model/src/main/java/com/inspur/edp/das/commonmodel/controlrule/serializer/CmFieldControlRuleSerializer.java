/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.controlrule.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.serializer.CommonFieldRuleSerializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.parser.CommonFieldRuleDefParser;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.serializer.CommonFieldRuleDefSerializer;
import com.inspur.edp.das.commonmodel.controlrule.CmFieldControlRule;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmFieldControlRuleDef;

public class CmFieldControlRuleSerializer<T extends CmFieldControlRule> extends CommonFieldRuleSerializer<T> {

    @Override
    protected final void writeCommonFieldRuleExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeCommonFieldRuleExtendInfos(controlRule, jsonGenerator, serializerProvider);
        writeCmFieldRuleExtendInfos(controlRule, jsonGenerator, serializerProvider);
    }

    protected void writeCmFieldRuleExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {


    }
}
