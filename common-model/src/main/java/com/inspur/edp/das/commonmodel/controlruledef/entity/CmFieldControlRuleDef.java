/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.controlruledef.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonFieldContrulRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.parser.CmFieldRuleDefParser;
import com.inspur.edp.das.commonmodel.controlruledef.serializer.CmFieldControlRuleDefSerializer;

@JsonSerialize(using = CmFieldControlRuleDefSerializer.class)
@JsonDeserialize(using = CmFieldRuleDefParser.class)
public class CmFieldControlRuleDef extends CommonFieldContrulRuleDef {
    public CmFieldControlRuleDef(ControlRuleDefinition parentRuleDefinition) {
        super(parentRuleDefinition, CmFieldRuleNames.CmFieldRuleObjectTypeName);
    }
}
