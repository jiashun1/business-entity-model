/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.entity.element.increment.extract;

import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonFieldControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonFieldContrulRuleDef;
import com.inspur.edp.cef.designtime.api.element.increment.ModifyFieldIncrement;
import com.inspur.edp.cef.designtime.api.element.increment.extract.ModifyFieldExtractor;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.cef.designtime.api.increment.extractor.ExtractUtils;
import com.inspur.edp.cef.designtime.api.increment.extractor.MonkControlRule;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.entity.GspCommonElement;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import lombok.var;

public class ModifyElementExtractor extends ModifyFieldExtractor {
    public ModifyElementExtractor(){
        super();
    }
    public ModifyElementExtractor(boolean includeAll){
        super(includeAll);
    }

    protected void extractExtendInfo(ModifyFieldIncrement increment, GspCommonField oldField, GspCommonField newField, CommonFieldControlRule rule, CommonFieldContrulRuleDef def) {

        extractCmExtendInfo(increment, (GspCommonElement)oldField, (GspCommonElement)newField, rule, def);
    }


    private void extractCmExtendInfo(ModifyFieldIncrement increment, GspCommonElement oldField, GspCommonElement newField, CommonFieldControlRule rule, CommonFieldContrulRuleDef def){

        if(includeAll)
            extractCmAllInfo(increment, oldField, newField);
    }

    private void extractCmAllInfo(ModifyFieldIncrement increment, GspCommonElement oldField, GspCommonElement newField){
        var changes = increment.getChangeProperties();
        ExtractUtils.extractValue(changes, oldField.getColumnID(), newField.getColumnID(), CommonModelNames.ColumnID, (ControlRuleItem)null, (ControlRuleDefItem)null);
        ExtractUtils.extractValue(changes, oldField.getBelongModelID(), newField.getBelongModelID(), CommonModelNames.BelongModelID, (ControlRuleItem)null, (ControlRuleDefItem)null);
        ExtractUtils.extractValue(changes, oldField.getReadonly(), newField.getReadonly(), CommonModelNames.Readonly, (ControlRuleItem)null, (ControlRuleDefItem)null);
        ExtractUtils.extractValue(changes, oldField.getIsCustomItem(), newField.getIsCustomItem(), CommonModelNames.IsCustomItem, (ControlRuleItem)null, (ControlRuleDefItem)null);

//        writeBillCodeConfig(writer, field.getBillCodeConfig());
    }

}
