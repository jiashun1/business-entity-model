/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.element.ElementCodeRuleConfig;
import com.inspur.edp.das.commonmodel.entity.element.GspCommonAssociation;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;

/**
 * The Definition Of Common Element
 *
 * @ClassName: GspCommonElement
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspCommonElement extends GspCommonField implements IGspCommonElement {
	public GspCommonElement() {
		billCodeConfig = new ElementCodeRuleConfig();
		setReadonly(false);
		setIsCustomItem(false);
	}

	// region 成员字段

	// 基础信息
	private transient ElementCodeRuleConfig billCodeConfig = new ElementCodeRuleConfig();
	@JsonIgnore
	private transient String columnid = "";
	private transient String belongModelId = "";
	private transient boolean readOnly;

	// 维护信息
	private boolean isCustomItem;

	// endregion 成员字段

	// region 公有属性

	/**
	 * 编码规则配置
	 * 
	 */
	@Override
	public ElementCodeRuleConfig getBillCodeConfig() {
		return billCodeConfig;
	}

	public void setBillCodeConfig(ElementCodeRuleConfig value) {
		billCodeConfig = value;
	}

	/**
	 * 对应的数据对象的列ID
	 * 
	 */
	public String getColumnID() {
		return columnid;
	}

	public void setColumnID(String value) {
		columnid = value;
	}

	/**
	 * 是否自定义项
	 * 
	 */
	public boolean getIsCustomItem() {
		return isCustomItem;
	}

	public void setIsCustomItem(boolean value) {

	}

	/**
	 * 当前属性所属结点
	 * 
	 */
	public IGspCommonObject getBelongObject() {
		return (IGspCommonObject) super.getBelongObject();
	}

	public void setBelongObject(IGspCommonObject value) {
		super.setBelongObject(value);
	}

	/**
	 * 所属数据模型metadataId
	 * 
	 */
	public String getBelongModelID() {
		return belongModelId;
	}

	public void setBelongModelID(String value) {
		value = belongModelId;
	}

	public boolean getReadonly() {
		return readOnly;
	}

	public void setReadonly(boolean value) {
		readOnly = value;
	}

	/**
	 * 克隆
	 * 
	 * @param absObj
	 * @param association
	 * @return
	 */
	public final IGspCommonElement clone(IGspCommonObject absObj, GspCommonAssociation association) {
		Object tempVar = super.clone(absObj, association);
		GspCommonElement newObj = (GspCommonElement) ((tempVar instanceof GspCommonElement) ? tempVar : null);
		if (newObj == null) {
			return null;
		}
		newObj.setBelongObject(absObj);
		newObj.setParentAssociation(association);
		if (getChildAssociations() != null) {
			newObj.setChildAssociations(new GspAssociationCollection());
			for (GspAssociation item : getChildAssociations()) {
				newObj.getChildAssociations().add(((GspCommonAssociation) item).clone(newObj));
			}
		}
		return newObj;
	}

	public final GspCommonAssociation getGspParentAssociation() {
		return (GspCommonAssociation) getParentAssociation();
	}

	public final void setGspParentAssociation(GspCommonAssociation value) {
		setParentAssociation(value);
	}

	@JsonIgnore
	public final String getAssociationTypeName() {
		if (getBelongObject().getParentObject() == null) {
			return getLabelID() + "Info";
		}
		return getBelongObject().getCode() + getLabelID() + "Info";
	}

	@Override
	@JsonIgnore
	public final String getEnumTypeName() {
		if (getBelongObject().getParentObject() == null) {
			return getLabelID() + "Enum";
		}
		return getBelongObject().getCode() + getLabelID() + "Enum";
	}
	// endregion
}
