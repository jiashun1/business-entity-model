/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.gspbusinessentity.config;

import com.inspur.edp.bef.bemanager.gspbusinessentity.GspBeExtendInfoService;
import com.inspur.edp.bef.bemanager.gspbusinessentity.repository.GspBeExtendInfoRepository;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * BE扩展信息注册服务
 *
 * @author hanll02
 */
@Configuration
@EntityScan({"com.inspur.edp.bef.bizentity.gspbusinessentity.entity"})
@EnableJpaRepositories({"com.inspur.edp.bef.bemanager.gspbusinessentity.repository"})
public class GspBeExtendInfoConfig {

  @Bean
  public GspBeExtendInfoService getBeExtendInfoService(GspBeExtendInfoRepository beRepository) {
    return new GspBeExtendInfoService(beRepository);
  }

}
