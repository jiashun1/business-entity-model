/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.config;

import com.inspur.edp.bef.bemanager.commonstructure.BEComStructureSchemaExtension;
import com.inspur.edp.bef.bemanager.service.BeManagerService;
import com.inspur.edp.caf.cef.dt.spi.CommonStructureSchemaExtension;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("com.inspur.edp.bef.bemanager.config.BefManagerConfig")
public class BefManagerConfig {

  @Bean("com.inspur.edp.bef.bemanager.config.BefManagerConfig.BEComStructureSchemaExtension")
  public CommonStructureSchemaExtension getBEComStructureSchemaExtension() {
    return new BEComStructureSchemaExtension();
  }

  @Bean
  @ConditionalOnClass(name = {"com.inspur.edp.jittojava.context.GenerateService"})
//TODO: tools目录中没有GenerateService,应拆分出去
//    @ConditionalOnBean(GenerateService.class)
  public BeManagerService getBizEntityDtService() {
    return new BeManagerService();
  }
}
