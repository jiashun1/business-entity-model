/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.validate.object;

import com.inspur.edp.bef.bemanager.validate.element.BizFieldChecker;
import com.inspur.edp.bef.bemanager.validate.operation.BizActionChecker;
import com.inspur.edp.bef.bemanager.validate.operation.TccActionChecker;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.operation.BizActionBase;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.internalmgraction.TccSettingElement;
import com.inspur.edp.cef.designtime.api.validate.common.CheckUtil;
import com.inspur.edp.cef.designtime.api.validate.element.FieldChecker;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.validate.object.CMObjectChecker;

public class BizObjChecker extends CMObjectChecker {

  private static BizObjChecker bizObject;

  public final static BizObjChecker getInstance() {
    if (bizObject == null) {
      bizObject = new BizObjChecker();
    }
    return bizObject;
  }

  @Override
  protected FieldChecker getFieldChecker() {
    return BizFieldChecker.getInstance();
  }

  @Override
  protected void checkAction(IGspCommonObject commonObject) {
    for (BizOperation bizAction : ((GspBizEntityObject) commonObject).getBizActions()) {
      BizActionChecker.getInstance().checkBizAction((BizActionBase) bizAction);
    }
  }

  @Override
  protected void checkExtension(IGspCommonObject commonObject) {
    for (TccSettingElement element : ((GspBizEntityObject) commonObject).getTccSettings()) {
      checkTccSettingElement(commonObject, element);
    }
  }

  private void checkTccSettingElement(IGspCommonObject commonObject, TccSettingElement ele) {
    checkBasicFieldInfo(commonObject, ele);
    checkTccAction(ele);

  }

  private void checkBasicFieldInfo(IGspCommonObject commonObject, TccSettingElement ele) {
    if (!CheckUtil.isLegality(ele.getCode())) {
      CheckUtil
          .exception("节点[" + commonObject.getCode() + "]的字段标签[" + ele.getCode() + "]是Java关键字,请修改！");
    }
  }

  private void checkTccAction(TccSettingElement ele) {
    TccActionChecker.getInstance().checkTccAction(ele.getTccAction());
  }

}
