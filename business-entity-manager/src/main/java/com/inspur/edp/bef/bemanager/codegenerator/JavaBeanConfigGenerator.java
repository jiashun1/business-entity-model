/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator;

import com.inspur.edp.bef.bemanager.util.CefConfigBeanGenCode;
import com.inspur.edp.bef.bemanager.util.ComponentExtendProperty;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import java.util.ArrayList;

public class JavaBeanConfigGenerator {

  private final ArrayList<ComponentExtendProperty> extendProperties;
  private final GspBusinessEntity be;
  private final String nameSpace;
  private final String suffix = "CefConfig";

  public JavaBeanConfigGenerator(GspBusinessEntity be,
      ArrayList<ComponentExtendProperty> properties, String nameSpace) {
    this.be = be;
    this.extendProperties = properties;
    this.nameSpace = nameSpace;
  }

  /**
   * 重新生成CefBeanConfig文件
   *
   * @return
   */
  public String generate() {
    StringBuilder strBuilder = new StringBuilder();

    // package
    String packageName = JavaCodeGeneratorUtil.ConvertImportPackage(be.getComponentAssemblyName());
    strBuilder.append(JavaCompCodeNames.KeywordPackage).append(" ").append(packageName).append(";")
        .append(JavaCodeGeneratorUtil.getNewline());

    // imports
    generateImports(strBuilder, JavaCompCodeNames.BeanPackage);
    generateImports(strBuilder, JavaCompCodeNames.ConfigurationPackage);

    // @Configuration()
    strBuilder.append("@").append(JavaCompCodeNames.KeywordConfiguration).append("()")
        .append(JavaCodeGeneratorUtil.getNewline());

    // class
    strBuilder.append(JavaCompCodeNames.KeywordPublic).append(" ")
        .append(JavaCompCodeNames.KeywordClass).append(" ").append(be.getCode()).append(suffix)
        .append("{").append(JavaCodeGeneratorUtil.getNewline());
    for (ComponentExtendProperty property : this.extendProperties) {
      generateBeanConfig(strBuilder, property);
    }

    strBuilder.append("}");
    return strBuilder.toString();
  }

  /**
   * 更新CefBeanConfig文件
   *
   * @param orgContent
   * @return
   */
  public String update(String orgContent) {
    // 当前文件为空，重新生成
    if (orgContent.indexOf("}") < 0) {
      return generate();
    }

    //当前文件不为空，在原有的Bean内容基础上进行添加
    String content = orgContent.substring(0, orgContent.lastIndexOf("}"));
    StringBuilder strBuilder = new StringBuilder(content);
    for (ComponentExtendProperty property : this.extendProperties) {
      generateBeanConfig(strBuilder, property);
    }

    strBuilder.append("}");
    return strBuilder.toString();
  }

  /**
   * 生成Import
   *
   * @param strBuilder
   */
  private void generateImports(StringBuilder strBuilder, String importName) {
    strBuilder.append(JavaCompCodeNames.KeywordImport).append(" ").append(importName).append(";")
        .append(JavaCodeGeneratorUtil.getNewline());
  }

  /**
   * 根据bean 文件信息生成单个注册代码
   *
   * @param strBuilder
   * @param property
   */
  private void generateBeanConfig(StringBuilder strBuilder, ComponentExtendProperty property) {
    CefConfigBeanGenCode genCodeInfo = property.getCefConfigBeanGenCode();
    strBuilder.append(JavaCodeGeneratorUtil.GetIndentationStr())
        .append(String.format(JavaCompCodeNames.KeyWordBean, genCodeInfo.getBeanCode())).append(" ")
        .append(JavaCompCodeNames.KeywordPublic).append(" ").append(genCodeInfo.getImportCode())
        .append(".").append(genCodeInfo.getClassName())
        .append(" get").append(genCodeInfo.getClassName()).append("() {")
        .append(JavaCodeGeneratorUtil.getNewline());

    strBuilder.append(JavaCodeGeneratorUtil.GetDoubleIndentationStr()).append(" ").
        append(JavaCompCodeNames.KeyWordReturn).append(" ").append(JavaCompCodeNames.KeyWordNew)
        .append(" ").
        append(genCodeInfo.getImportCode()).append(".").append(genCodeInfo.getClassName())
        .append("();").append(JavaCodeGeneratorUtil.getNewline()).
        append(JavaCodeGeneratorUtil.GetIndentationStr()).append("}")
        .append(JavaCodeGeneratorUtil.getNewline());
  }
}
