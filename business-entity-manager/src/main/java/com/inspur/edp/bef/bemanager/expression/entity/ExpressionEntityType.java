/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.expression.entity;

import java.util.ArrayList;
import java.util.List;

public class ExpressionEntityType {

  private String id;
  private String name;
  private String description;
  private String key;
  private List<ExpressionProperty> properties;
  private List<NavigationProperty> navigationProperties;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public List<ExpressionProperty> getProperties() {

    if (properties == null) {
      properties = new ArrayList<>();
    }
    return properties;
  }

  public void setProperties(
      List<ExpressionProperty> properties) {
    this.properties = properties;
  }

  public List<NavigationProperty> getNavigationProperties() {
    if (navigationProperties == null) {
      navigationProperties = new ArrayList<>();
    }
    return navigationProperties;
  }

  public void setNavigationProperties(
      List<NavigationProperty> navigationProperties) {
    this.navigationProperties = navigationProperties;
  }

  public ExpressionEntityType clone() {
    ExpressionEntityType newType = new ExpressionEntityType();
    newType.setId(getId());
    newType.setDescription(getDescription());
    newType.setKey(getKey());
    newType.setName(getName());
    getProperties().forEach(item -> {
      newType.getProperties().add(item.clone());
    });
    getNavigationProperties().forEach(item -> {
      newType.getNavigationProperties().add(item.clone());
    });
    return newType;
  }
}
