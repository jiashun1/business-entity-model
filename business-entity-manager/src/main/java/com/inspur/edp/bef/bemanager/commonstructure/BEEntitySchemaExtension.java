/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.commonstructure;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.caf.cef.rt.api.CommonStructureInfo;
import com.inspur.edp.caf.cef.rt.spi.EntitySchemaExtension;
import com.inspur.edp.caf.cef.schema.structure.CommonStructure;
import com.inspur.edp.lcm.metadata.api.service.MetadataRTService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class BEEntitySchemaExtension implements EntitySchemaExtension {

  @Override
  public CommonStructure getEntity(String metaId) {
    MetadataRTService service = SpringBeanUtils.getBean(MetadataRTService.class);
//		MetadataRTService service = CefDesignTimeBeanUtil.getAppCtx().getBean(MetadataRTService.class);
    GspBusinessEntity be = (GspBusinessEntity) service.getMetadata(metaId).getContent();
    return (CommonStructure) be;
  }

  @Override
  public CommonStructureInfo getEntitySummary(String s) {
    return null;
  }

  @Override
  public String getEntityType() {
    return "GSPBusinessEntity";
  }
}
