/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.befdtconsistencycheckevent;

import com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs.ChangingActionCodeEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs.ChangingActionParamsEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs.ChangingActionReturnEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs.DeletingActionEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs.ChangingEntityCodeEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs.RemovingEntityEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs.ChangingFieldDataTypeEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs.ChangingFieldLabelIdEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs.ChangingFieldObjectTypeEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs.RemovingFieldEventArgs;
import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class BefDtConsistencyCheckEventBroker extends EventBroker {

  private BefDtConsistencyCheckEventManager eventManager;

  public BefDtConsistencyCheckEventBroker(EventListenerSettings settings,BefDtConsistencyCheckEventManager eventManager) {
    super(settings);
    this.eventManager = eventManager;
    this.init();
  }

  @Override
  protected void onInit() {
    this.getEventManagerCollection().add(eventManager);
  }


  public void fireRemovingEntity(RemovingEntityEventArgs args) {
    eventManager.fireRemovingEntity(args);
  }

  public void fireRemovingField(RemovingFieldEventArgs args) {
    eventManager.fireRemovingField(args);
  }

  public void fireDeletingAction(DeletingActionEventArgs args) {
    eventManager.fireDeletingAction(args);
  }
}
