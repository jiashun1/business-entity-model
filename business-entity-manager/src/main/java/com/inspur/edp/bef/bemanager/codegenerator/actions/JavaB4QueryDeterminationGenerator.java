/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator.actions;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizOperation;

public class JavaB4QueryDeterminationGenerator extends JavaDeterminationGenerator {

  @Override
  protected String getBaseClassName() {
    return "AbstractQueryDetermination";
  }

  public JavaB4QueryDeterminationGenerator(GspBusinessEntity be, BizOperation operation,
      String nameSpace, String path) {
    super(be, operation, nameSpace, path);

  }

  @Override
  protected String GetNameSpaceSuffix() {
    return BizOperation.getOwner().getCode() + "." + JavaCompCodeNames.DeterminationNameSpaceSuffix;
  }

  @Override
  protected void JavaGenerateExtendUsing(StringBuilder result) {
    result.append(GetImportStr(JavaCompCodeNames.ActionApiNameSpace));
    result.append(GetImportStr(JavaCompCodeNames.DaterminationNameSpace));
    result.append(GetImportStr(JavaCompCodeNames.ChangesetNameSpace));
    result.append(GetImportStr(JavaCompCodeNames.AbstractDeterminationNameSpace));
    result.append(GetImportStr(JavaCompCodeNames.IDeterminationContextNameSpace));
  }

  @Override
  protected void JavaGenerateConstructor(StringBuilder result) {
    result.append(GetDoubleIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ")
        .append(GetCompName()).append("(IQueryDeterminationContext context)").append(" ")
        .append("{").append(getNewline());
    result.append(GetDoubleIndentationStr()).append(GetIndentationStr()).append("super(context)")
        .append(";").append(getNewline());
    result.append(GetDoubleIndentationStr()).append("}");
  }

  @Override
  protected void JavaGenerateExtendMethod(StringBuilder result) {
  }

  @Override
  protected String GetInitializeCompName() {
    return String.format("%1$s%2$s%3$s", getChildCode(), BizOperation.getCode(), "Determination");
  }
}
