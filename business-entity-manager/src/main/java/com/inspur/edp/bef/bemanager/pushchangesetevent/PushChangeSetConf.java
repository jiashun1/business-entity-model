/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.pushchangesetevent;

import io.iec.edp.caf.commons.event.config.EventListenerSettings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PushChangeSetConf {

  @Bean(name = "PushChangeSetEventBroker")
  public PushChangeSetEventBroker setEventBroker(EventListenerSettings settings,
      PushChangeSetEventManager eventManager) {
    return new PushChangeSetEventBroker(settings, eventManager);
  }

  @Bean(name = "PushChangeSetEventManager")
  public PushChangeSetEventManager setEventManager() {
    return new PushChangeSetEventManager();
  }
}
