/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator;

public class JavaCodeGeneratorUtil {

  public static String ConvertImportPackage(String readPropertyValue_string) {
    String[] strArray = readPropertyValue_string.split("[.]", -1);
    String str = "com.";
    int i;

    for (i = 0; i < strArray.length - 1; i++) {
      str += strArray[i].toLowerCase() + ".";
    }
    str += strArray[i].toLowerCase();
    return str;
  }

  public static String getNewline() {
    return "\r\n";
  }

  public static String GetIndentationStr() {
    return "\t";
    //return "    ";
  }

  public static String GetDoubleIndentationStr() {
    return "\t\t";
  }
}
