/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.entity.increment.extractor;

import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonDataTypeControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeControlRuleDef;
import com.inspur.edp.cef.designtime.api.entity.GspCommonDataType;
import com.inspur.edp.cef.designtime.api.entity.increment.CommonEntityIncrement;
import com.inspur.edp.cef.designtime.api.increment.extractor.AbstractIncrementExtractor;

public class DataTypeIncrementExtractor extends AbstractIncrementExtractor {

    protected boolean includeAll;

    public DataTypeIncrementExtractor(){

    }

    public DataTypeIncrementExtractor(boolean includeAll){

        this.includeAll = includeAll;
    }
    public CommonEntityIncrement extractorIncrement(
            GspCommonDataType oldDataType,
            GspCommonDataType newDataType,
            CommonDataTypeControlRule rule,
            CommonDataTypeControlRuleDef def){
//        CommonEntityIncrement increment = createCommonEntityIncrement();

        if(oldDataType == null && newDataType == null)
            return null;
        if(oldDataType == null && newDataType != null)
            return getAddedEntityExtractor().extract(newDataType);
        if(oldDataType != null && newDataType == null)
            return getDeletedEntityExtractor().extract(oldDataType);
        return getModifyEntityExtractor().extract(oldDataType, newDataType, rule, def);

    }

    protected AddedEntityExtractor getAddedEntityExtractor(){
        return new AddedEntityExtractor();
    }

    protected DeletedEntityExtractor getDeletedEntityExtractor(){return new DeletedEntityExtractor();}

    protected ModifyEntityExtractor getModifyEntityExtractor(){ return new ModifyEntityExtractor(includeAll);}
}
