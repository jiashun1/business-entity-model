/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.validate.object;

import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.validate.element.FieldChecker;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class CommonDataTypeChecker {
    //实体编号
    private void checkNodeCode(IGspCommonDataType gspCommonDataType) {
//        if(StringUtils.isEmpty(gspCommonDataType.getCode())){
//            CheckUtil.exception(String.format("名称为[%s]的对象编号不允许为空！", gspCommonDataType.getName()));
//        }
//        if(StringUtils.isEmpty(gspCommonDataType.getName())){
//            CheckUtil.exception(String.format("编号为[%s]的对象名称不允许为空！", gspCommonDataType.getCode()));
//        }
//        if (!CheckUtil.isLegality(gspCommonDataType.getCode())){
//            CheckUtil.exception("对象编号[" + gspCommonDataType.getCode() + "]是Java关键字,请修改！");
//        }
    }

    public final void checkCDT(IGspCommonDataType commonDataType) {
        checkNodeCode(commonDataType);
        checkElements(commonDataType);
        checkExtentInfo(commonDataType);
    }

    protected void checkExtentInfo(IGspCommonDataType commonDataType) {
    }

    private void checkElements(IGspCommonDataType commonDataType) {
        if (commonDataType == null) return;
        for (IGspCommonField field : commonDataType.getContainElements()) {
            checkField(field, commonDataType);
        }
    }

    //校验字段
    private  void checkField(IGspCommonField field, IGspCommonDataType gspCommonDataType){
       getFieldChecker().checkField(field, gspCommonDataType);
    };
    protected abstract FieldChecker getFieldChecker();
}
