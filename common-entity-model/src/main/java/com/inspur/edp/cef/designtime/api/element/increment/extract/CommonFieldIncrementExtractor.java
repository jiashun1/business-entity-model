/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.element.increment.extract;

import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonFieldControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonFieldContrulRuleDef;
import com.inspur.edp.cef.designtime.api.element.increment.GspCommonFieldIncrement;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.cef.designtime.api.increment.extractor.AbstractIncrementExtractor;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonFieldIncrementExtractor extends AbstractIncrementExtractor {

    protected boolean includeAll;

    public CommonFieldIncrementExtractor(){

    }

    public CommonFieldIncrementExtractor(boolean includeAll){
        this.includeAll = includeAll;
    }

    public GspCommonFieldIncrement extractorIncrement(
            GspCommonField oldField, GspCommonField newField, CommonFieldControlRule rule, CommonFieldContrulRuleDef def){

        if(oldField == null && newField == null)
            return null;
        if(oldField == null && newField != null)
            return getAddedFieldExtractor().extract(newField);
        if(oldField != null && newField == null)
            return getDeletedFieldExtractor().extract(oldField);
        return getModifyFieldExtractor().extract(oldField, newField, rule, def);
    }


    protected AddedFieldExtractor getAddedFieldExtractor(){
        return new AddedFieldExtractor();
    }

    protected DeletedFieldExtractor getDeletedFieldExtractor(){
        return new DeletedFieldExtractor();
    }

    protected ModifyFieldExtractor getModifyFieldExtractor(){
        return new ModifyFieldExtractor(includeAll);
    }
}
