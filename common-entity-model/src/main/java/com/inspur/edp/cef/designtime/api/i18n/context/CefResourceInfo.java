/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.i18n.context;

import com.inspur.edp.cef.designtime.api.util.DataValidator;
/**
 * 国际化资源信息
 */
public class CefResourceInfo {

	public CefResourceInfo(CefResourcePrefixInfo prefixInfo, String resourceKeyPropName, String descriptionPropName, String resourceValue) {
		DataValidator.checkForNullReference(prefixInfo, "PrefixInfo");
		DataValidator.checkForEmptyString(prefixInfo.getDescriptionPrefix(), "DescriptionPrefix");
		DataValidator.checkForEmptyString(prefixInfo.getResourceKeyPrefix(), "ResourceKeyPrefix");

		DataValidator.checkForEmptyString(resourceKeyPropName, "ResourceKeyPropName");
		DataValidator.checkForEmptyString(descriptionPropName, "DescriptionPropName");
		DataValidator.checkForEmptyString(resourceValue, "ResourceValue");

        this.prefixInfo = prefixInfo;
		this.resourceKeyPropName = resourceKeyPropName;
		this.descriptionPropName = descriptionPropName;
		this.resourceValue = resourceValue;
	}

	private CefResourcePrefixInfo prefixInfo;
	public final CefResourcePrefixInfo getPrefixInfo() {
		return prefixInfo;
	}

	/**
	 属性名
	 e.g. Name

	*/
	private String resourceKeyPropName;
	public final String getResourceKeyPropName() {
		return resourceKeyPropName;
	}

	/**
	 描述中属性名
	 e.g. 名称

	*/
	private String descriptionPropName;
	public final String getDescriptionPropName() {
		return descriptionPropName;
	}

	/**
	 默认语言的值

	*/
	private String resourceValue;
	public final String getResourceValue() {
		return resourceValue;
	}

	public String getResourceKey(){
	    return getPrefixInfo().getResourceKeyPrefix() +"."+ getResourceKeyPropName();
    }

	public String getDescription(){
        return getPrefixInfo().getDescriptionPrefix() + "的" + getDescriptionPropName();
    }
}
