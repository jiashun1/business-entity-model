/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.util;

import com.inspur.edp.metadata.rtcustomization.api.CustomizationRtService;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CefDtBeanUtil {

  private static CustomizationService customizationService;

  public static CustomizationService getCustomizationService() {
    if (customizationService == null) {
      customizationService = SpringBeanUtils.getBean(CustomizationService.class);
    }
    return customizationService;
  }

  private static CustomizationRtService customizationRtService;

  public static CustomizationRtService getCustomizationRtService() {
    if (customizationRtService == null) {
      customizationRtService = SpringBeanUtils.getBean(CustomizationRtService.class);
    }
    return customizationRtService;
  }
}
