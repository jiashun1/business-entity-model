/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.parser.RangeRuleDefParser;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeControlRuleDef;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeRuleNames;

/**
 * The Json Parser Of CommonDataTypeRuleDef
 *
 * @ClassName: CommonDataTypeRuleDefParser
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonDataTypeRuleDefParser<T extends CommonDataTypeControlRuleDef> extends RangeRuleDefParser<T> {
    @Override
    protected final boolean readRangeExtendControlRuleProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        if(super.readRangeExtendControlRuleProperty(ruleDefinition, propName, jsonParser, deserializationContext))
            return true;
        return readCommonDataTypeRuleExtendProperty(ruleDefinition,propName,jsonParser,deserializationContext);
    }

    protected boolean readCommonDataTypeRuleExtendProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        return false;
    }

    @Override
    protected final T createRuleDefinition() {
        return (T) createCommonDataTypeRuleDefinition();
    }

    protected CommonDataTypeControlRuleDef createCommonDataTypeRuleDefinition() {
        return new CommonDataTypeControlRuleDef(null, CommonDataTypeRuleNames.RuleObjectType);
    }
}
