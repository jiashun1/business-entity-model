/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.validate.element;

import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.validate.common.CheckUtil;

import static com.inspur.edp.cef.designtime.api.validate.common.CheckUtil.isLegality;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class AssociationFieldChecker {
    private static AssociationFieldChecker associationFieldChecker;

    public static AssociationFieldChecker getInstance() {
        if (associationFieldChecker == null) {
            associationFieldChecker = new AssociationFieldChecker();
        }
        return associationFieldChecker;
    }

    public final void checkAssociationField(IGspCommonField field, IGspCommonDataType commonDataType) {
        for (GspAssociation association : field.getChildAssociations()) {
            for (IGspCommonField refField : association.getRefElementCollection()) {
                if (!isLegality(refField.getLabelID())) {
                    CheckUtil.exception("节点["+commonDataType.getCode()+"]的关联字段标签[" + refField.getLabelID()+ "]是Java关键字,请修改！");
                }
            }
        }
    }
}
