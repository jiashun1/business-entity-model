/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm;

/**
 * The Serlializer Names Of CommonFieldRule
 *
 * @ClassName: CommonFieldRuleNames
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonFieldRuleNames {
    public static String Code="Code";
    public static String Name="Name";
    public static String Length="Length";
    public static String Precision="Precision";
    public static String DefaultValue="DefaultValue";
    public static String ObjectType="ObjectType";
    public static String MultiLanField="MultiLanField";
    public static String Readonly="Readonly";
    public static String Required="Required";
    public static String RuleObjectType="CommonField";
}
