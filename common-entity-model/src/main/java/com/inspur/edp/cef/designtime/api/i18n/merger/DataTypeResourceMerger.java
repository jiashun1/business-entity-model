/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.i18n.merger;

import com.inspur.edp.cef.designtime.api.IFieldCollection;
import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.cef.designtime.api.i18n.names.CefResourceKeyNames;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;

import static com.inspur.edp.cef.designtime.api.increment.merger.MergeUtils.getKeyPrefix;

public  abstract class DataTypeResourceMerger extends AbstractResourceMerger {

  private IGspCommonDataType dataType;

  protected DataTypeResourceMerger(IGspCommonDataType commonDataType, ICefResourceMergeContext context) {
    super(context);
    this.dataType=commonDataType;
  }

  @Override
  protected void mergeItems() {
    I18nResourceItemCollection resourceItems=getContext().getResourceItems();
    String keyPrefix=getKeyPrefix(dataType.getI18nResourceInfoPrefix(), CefResourceKeyNames.Name);
    dataType.setName(resourceItems.getResourceItemByKey(keyPrefix).getValue());
    extractDataTypeFields(dataType.getContainElements());
    //扩展
    extractExtendProperties(dataType);
  }

  private void extractDataTypeFields(IFieldCollection fields) {
    if (fields == null || fields.size() == 0) {
      return;
    }
    for (IGspCommonField field : fields) {
      CefFieldResourceMerger merger = getCefFieldResourceMerger(getContext(), field);
      merger.merge();
    }
  }

  protected abstract CefFieldResourceMerger getCefFieldResourceMerger(
      ICefResourceMergeContext context, IGspCommonField field);


  protected void extractExtendProperties(IGspCommonDataType dataType) {
  }

}
