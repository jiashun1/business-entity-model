/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.json.object;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.collection.CommonDtmCollection;
import com.inspur.edp.cef.designtime.api.collection.CommonValCollection;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldSerializer;
import com.inspur.edp.cef.designtime.api.json.operation.CommonDtmSerializer;
import com.inspur.edp.cef.designtime.api.json.operation.CommonValSerializer;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;

/**
 * The Json Serializer Of CommonDataType
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class GspCommonDataTypeSerializer extends JsonSerializer<IGspCommonDataType> {

    protected boolean isFull = true;
    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        if(flag == null)
            return;
        this.flag = flag;
    }

    private String flag = "V1";
    private CommonDtmSerializer dtmSerializer;
    private CommonValSerializer valSerializer;
    public GspCommonDataTypeSerializer(){
//        dtmSerializer = new CommonDtmSerializer(isFull);
//        valSerializer = new CommonValSerializer(isFull);
    }
    public GspCommonDataTypeSerializer(boolean full){
        isFull = full;
//        dtmSerializer = new CommonDtmSerializer(isFull);
//        valSerializer = new CommonValSerializer(isFull);
    }



    @Override
    public void serialize(IGspCommonDataType dataType, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        SerializerUtils.writeStartObject(jsonGenerator);
        writeBaseObjInfo(dataType, jsonGenerator);
        writeSelfObjInfo(dataType,jsonGenerator);
        SerializerUtils.writeEndObject(jsonGenerator);
    }


    //region BaseProp
    private void writeBaseObjInfo(IGspCommonDataType dataType, JsonGenerator jsonGenerator){
        SerializerUtils.writePropertyValue(jsonGenerator, CefNames.ID,dataType.getID());
        SerializerUtils.writePropertyValue(jsonGenerator, CefNames.Code,dataType.getCode());
        SerializerUtils.writePropertyValue(jsonGenerator, CefNames.Name,dataType.getName());
        if(isFull||dataType.getIsRef()){
            SerializerUtils.writePropertyValue(jsonGenerator, CefNames.IsRef,dataType.getIsRef());
        }
        SerializerUtils.writePropertyValue(jsonGenerator, CefNames.I18nResourceInfoPrefix,dataType.getI18nResourceInfoPrefix());
        if(isFull||dataType.getCustomizationInfo().isCustomized())
        SerializerUtils.writePropertyValue(jsonGenerator, CefNames.CustomizationInfo,dataType.getCustomizationInfo());
        if (isFull||(dataType.getContainElements() != null&&dataType.getContainElements().size()>0)) {
            SerializerUtils.writePropertyName(jsonGenerator, CefNames.ContainElements);
            SerializerUtils.writeArray(jsonGenerator, getFieldSerializer(),dataType.getContainElements());
        }
        if(isFull||(dataType.getBeLabel()!=null&&dataType.getBeLabel().size()>0)){
            SerializerUtils.writePropertyValue(jsonGenerator, CefNames.BeLabel,dataType.getBeLabel());
        }
        if(isFull||(dataType.getBizTagIds()!=null&&dataType.getBizTagIds().size()>0)){
            SerializerUtils.writePropertyValue(jsonGenerator, CefNames.BizTagIds,dataType.getBizTagIds());
        }
        writeExtendCdtBaseProperty(dataType , jsonGenerator);

    }

    //endregion

    //region SelfProp
    private void writeSelfObjInfo(IGspCommonDataType dataType, JsonGenerator writer){
        //不需要了
        writeDtmAfterModify(dataType,writer);
        writeDtmBeforeSave(dataType,writer);
        writeDtmAfterCreate(dataType,writer);
        writeValAfterModify(dataType,writer);
        writeValB4Save(dataType,writer);
        writeExtendCdtSelfProperty(dataType, writer);

    }

    protected final void writeDtm(String propName, CommonDtmCollection collection, JsonGenerator writer){
        if (collection == null || collection.size() < 1)
            return;
        SerializerUtils.writePropertyName(writer,propName);
        SerializerUtils.WriteStartArray(writer);
        for (CommonDetermination commonDetermination : collection)
            getCommonDtmSerializer(this.isFull).serialize(commonDetermination,writer,  null);
        SerializerUtils.WriteEndArray(writer);
    }

    public CommonDtmSerializer getCommonDtmSerializer(boolean isFull){
        return new CommonDtmSerializer(isFull);
    }

    private void writeDtmAfterModify(IGspCommonDataType dataType, JsonGenerator writer){
        writeDtm(CefNames.DtmAfterModify, dataType.getDtmAfterModify(),writer);
    }

    private void writeDtmBeforeSave(IGspCommonDataType dataType,JsonGenerator writer) {
        writeDtm(CefNames.DtmBeforeSave, dataType.getDtmBeforeSave(), writer);
    }

    private void writeDtmAfterCreate(IGspCommonDataType dataType,JsonGenerator writer) {
        writeDtm(CefNames.DtmAfterCreate, dataType.getDtmAfterCreate(), writer);
    }

    private void writeValAfterModify(IGspCommonDataType dataType, JsonGenerator writer){
        writeVal(CefNames.ValAfterModify, dataType.getValAfterModify(), writer);
    }

    private void writeValB4Save(IGspCommonDataType dataType, JsonGenerator writer){
        writeVal(CefNames.ValBeforeSave, dataType.getValBeforeSave(), writer);
    }


    protected final void writeVal(String propName, CommonValCollection collection, JsonGenerator writer){
        if (collection == null || collection.size() < 1)
            return;
        SerializerUtils.writePropertyName(writer, propName);
        SerializerUtils.WriteStartArray(writer);
        for (CommonValidation commonValidation : collection)
            getCommonValSerializer().serialize(commonValidation,writer,  null);
        SerializerUtils.WriteEndArray(writer);
    }
    //endregion

    protected CommonValSerializer getCommonValSerializer(){
        return new CommonValSerializer(this.isFull);
    }

    //region 抽象方法
    protected void writeExtendCdtBaseProperty(IGspCommonDataType commonDataType, JsonGenerator jsonGenerator){

    }
    protected abstract CefFieldSerializer getFieldSerializer();

    protected abstract void writeExtendCdtSelfProperty(IGspCommonDataType info,JsonGenerator jsonGenerator);

    //endregion
}
