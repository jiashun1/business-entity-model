/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.util;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;
import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataQueryParam;
import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataScopeEnum;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class MetadataUtil {

    private static CustomizationService customizationService =  SpringBeanUtils.getBean(CustomizationService.class);

    public static GspMetadata getCustomMetadata(String id){
        MetadataQueryParam param = new MetadataQueryParam();
        param.setMetadataId(id);
        param.setMetadataScopeEnum(MetadataScopeEnum.CUSTOMIZING);
        return customizationService.getGspMetadata(param);
    }

    public static GspMetadata getCustomMetadataByI18n(String id, boolean i18n){
        MetadataQueryParam param = new MetadataQueryParam();
        param.setMetadataId(id);
        param.setIsI18n(i18n);
        param.setMetadataScopeEnum(MetadataScopeEnum.CUSTOMIZING);
        return customizationService.getGspMetadata(param);
    }

    public static GspMetadata getCustomRTMetadata(String id){
        MetadataQueryParam param = new MetadataQueryParam();
        param.setMetadataId(id);
        param.setMetadataScopeEnum(MetadataScopeEnum.RUNTIME);
        return customizationService.getGspMetadata(param);
    }

    public static GspMetadata getCustomRTMetadataByI18n(String id,boolean i18n){
        MetadataQueryParam param = new MetadataQueryParam();
        param.setMetadataId(id);
        param.setIsI18n(i18n);
        param.setMetadataScopeEnum(MetadataScopeEnum.RUNTIME);
        return customizationService.getGspMetadata(param);
    }

}
