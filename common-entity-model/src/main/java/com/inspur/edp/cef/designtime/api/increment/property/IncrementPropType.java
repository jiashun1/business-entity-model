/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.increment.property;

import java.util.HashMap;

public enum IncrementPropType {
    String(0),
    Int(1),
    Decimal(2),
    Boolean(3),
    Date(4),
    Object(5);


    private int intValue;
    private static HashMap<Integer, IncrementPropType> mappings;
    private synchronized static HashMap<Integer, IncrementPropType> getMappings() {
        if (mappings == null) {
            mappings = new HashMap();
        }
        return mappings;
    }

    private IncrementPropType(int value) {
        intValue = value;
        IncrementPropType.getMappings().put(value, this);
    }

    public int getValue()
    {
        return intValue;
    }

    public static IncrementPropType forValue(int value)
    {
        return getMappings().get(value);
    }

}
