/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.json.Variable;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.cef.designtime.api.collection.GspFieldCollection;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldDeserializer;
import com.inspur.edp.cef.designtime.api.variable.CommonVariable;
import com.inspur.edp.cef.designtime.api.variable.CommonVariableCollection;
import com.inspur.edp.cef.designtime.api.variable.VariableSourceType;

/**
 * The Json Parser Of Common Variable
 *
 * @ClassName: CommonVariableDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonVariableDeserializer extends CefFieldDeserializer {
    @Override
    protected boolean readExtendFieldProperty(GspCommonField item, String propName, JsonParser jsonParser) {
        boolean result = true;
        CommonVariable field = (CommonVariable) item;
        switch (propName) {
            case CefNames.VariableSourceType:
                field.setVariableSourceType(SerializerUtils.readPropertyValue_Enum(jsonParser, VariableSourceType.class, VariableSourceType.values(), VariableSourceType.BE));
                break;
            default:
                result = false;
        }
        return result;
    }


    @Override
    protected void beforeCefElementDeserializer(GspCommonField item) {

    }

    @Override
    protected GspFieldCollection CreateFieldCollection() {
        return new CommonVariableCollection(null);
    }

    @Override
    protected GspCommonField CreateField() {
        return new CommonVariable();
    }
}
